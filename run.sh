#!/bin/bash

set -Eeuo pipefail
shopt -s inherit_errexit

if [[ -v TRIGGER_CONFIG_URL ]]; then
    TRIGGER_CONFIG_FILENAME="${TRIGGER_CONFIG_URL##*/}"
    # shellcheck disable=SC2154
    curl \
        --config "${CKI_CURL_CONFIG_FILE}" \
        --output "/data/${TRIGGER_CONFIG_FILENAME}" \
        "${TRIGGER_CONFIG_URL}"
fi

# shellcheck disable=SC2154
export START_PYTHON_TRIGGER="triggers ${TRIGGER_TYPE} ${TRIGGER_OPTIONS:-} -c /data/${TRIGGER_CONFIG_FILENAME}"
export LOG_NAME="${TRIGGER_TYPE}"
if [[ -v TRIGGER_SCHEDULE ]]; then
    # shellcheck disable=SC2086
    python3 -m ${START_PYTHON_TRIGGER} 2>&1 | tee -a "/logs/${LOG_NAME}.log"
else
    exec cki_entrypoint.sh
fi
