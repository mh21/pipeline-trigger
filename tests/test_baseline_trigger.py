"""Tests for triggers.baseline_trigger."""
import os
import unittest
from unittest import mock

from cki_lib import config_tree
from cki_lib import misc
import freezegun

from triggers import baseline_trigger


class TestLoadTriggers(unittest.TestCase):
    """Tests for baseline_trigger.load_triggers()."""

    @freezegun.freeze_time("2019-01-01")
    @mock.patch('triggers.utils.get_commit_hash', mock.Mock(return_value='current_baseline'))
    def test_trigger(self):
        cases = (
            ('pipeline project', {'cki_project': 'username/project'}, False, {}, [{}]),
            ('parent project', {'cki_pipeline_project': 'project'}, False, {
                'GITLAB_PARENT_PROJECT': 'username'
            }, [{}]),
            ('was tested', {'cki_project': 'username/project'}, True, {}, []),
            ('variant kernel', {
                'cki_project': 'username/project',
                'package_name': 'kernel-debug',
            }, False, {}, [{
                'package_name': 'kernel-debug',
                'title': 'Baseline: name main:current_base (kernel-debug)',
            }]),
        )

        for description, trigger_config, was_tested, environ, expected_triggers in cases:
            with (self.subTest(description),
                  mock.patch.dict(os.environ, environ),
                  mock.patch('triggers.utils.was_tested', mock.Mock(return_value=was_tested))):
                config = config_tree.process_config_tree({'test_name': {
                    'name': 'name',
                    'git_url': 'http://git.test/git/kernel.git',
                    '.branches': ['main'],
                    'cki_pipeline_branch': 'test_name',
                    '.report_rules': [{'if': 'something',
                                       'send_cc': 'someone',
                                       'send_bcc': 'someone_else'}],
                    **trigger_config,
                }})

                triggers = baseline_trigger.load_triggers(mock.Mock(), config, False)

                self.assertEqual(triggers, [{
                    'git_url': 'http://git.test/git/kernel.git',
                    'watch_url': 'http://git.test/git/kernel.git',
                    'name': 'name',
                    'commit_hash': 'current_baseline',
                    'watched_repo_commit_hash': 'current_baseline',
                    'branch': 'main',
                    'watch_branch': 'main',
                    'cki_pipeline_branch': 'test_name',
                    'cki_project': 'username/project',
                    'title': 'Baseline: name main:current_base',
                    'discovery_time': misc.utc_now_iso(),
                    'report_rules': (
                        '[{"if": "something", "send_cc": "someone", "send_bcc": "someone_else"}]'
                    ),
                    **e,
                } for e in expected_triggers])
